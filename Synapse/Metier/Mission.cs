﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    class Mission
    {
        private string _nom;
        private string _description;
        private int _nbHeuresPrévues;
        private Dictionary<DateTime, int> _releveHorraire;
        private Intervenant executant;


        public Dictionary<DateTime, int> ReleveHorraire { get => _releveHorraire; set => _releveHorraire = value; }
        public int NbHeuresPrévues { get => _nbHeuresPrévues; set => _nbHeuresPrévues = value; }
        public string Description { get => _description; set => _description = value; }
        public string Nom { get => _nom; set => _nom = value; }
        internal Intervenant Executant { get => executant; set => executant = value; }

        public void AjouteReleve(DateTime uneDate, int nbHeures)
        {
            this.ReleveHorraire.Add(uneDate, nbHeures);

        }
        
    }
}
