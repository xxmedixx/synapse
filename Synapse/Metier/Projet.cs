﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    class Projet
    {
        private string _nom;
        private DateTime _debut;
        private DateTime _fin;
        private decimal _prixfactureMO;
        private List<Mission> missions;

        public decimal PrixfactureMO { get => _prixfactureMO; set => _prixfactureMO = value; }
        public DateTime Fin { get => _fin; set => _fin = value; }
        public DateTime Debut { get => _debut; set => _debut = value; }
        public string Nom { get => _nom; set => _nom = value; }
        internal List<Mission> Missions { get => missions; set => missions = value; }

        private decimal CumulCountMo()
        {
            decimal cumul = 0;
            foreach ( Mission m in missions)
            {
                cumul += m.NbHeuresPrévues * m.Executant.TauxHoraire;
            
            }
            return cumul;
        }

        public decimal MargeBruteCourante()
        {
            decimal marge = 0;
            marge = this.PrixfactureMO - this.CumulCountMo();
            return marge;
        }
    }

}
